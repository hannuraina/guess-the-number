var HtmlWebpackPlugin = require('html-webpack-plugin');
var BrowserSyncPlugin = require('browser-sync-webpack-plugin');
var HistoryFallback   = require('connect-history-api-fallback');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var path              = require('path')

var HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: __dirname + '/app/index.html',
  filename: 'index.html',
  inject: 'body'
});

module.exports = {
  entry: [
    './app/js/app.js'
  ],
  output: {
    filename: 'app.js',
    path: path.join(__dirname, './dist')
  },
  module: {
    preLoaders: [
      { test: /\.js$/, include: path.join(__dirname, './app/js'), loader: 'eslint-loader' }
    ],
    loaders: [
      { test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel-loader' },
      { test: /\.scss$/, include: /app/, loader: ExtractTextPlugin.extract('css!sass')}
    ]
  },
  plugins: [
    HtmlWebpackPluginConfig,
    new ExtractTextPlugin('app.css'),
    new BrowserSyncPlugin({
      server: {
  			baseDir: 'dist',
  			middleware: [ HistoryFallback() ]
  		}
    })
  ]
};
