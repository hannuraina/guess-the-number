import React  from 'react';
import Slider from 'rc-slider';

// max range for settings selection slider
const MIN              = 0;
const MAX              = 100000;
const ERR_NOT_A_NUMBER = ' is not a number';
const ERR_OUT_OF_RANGE = ' is out of range';
const ERR_EMPTY        = ' enter a number';
const CORRECT          = ' is correct';
const INCORRECT        = ' is not correct';


class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      range:        [MIN, MAX],
      status:       null,
      showSettings: false
    };
    this.random = this.randomize(); // set default random number
  }

  handleSliderChange(value) {
    this.random = this.randomize();
    this.setState({ range: [value[0], value[1]] });
  }

  handleToggleSettings () {
    this.setState({ showSettings: !this.state.showSettings });
  }

  handleGuess (evt) {
    evt.preventDefault();
    this.setState({
      status: this.validate(this.refs.guess.value)
    });
    this.refs.guess.value = null;
    this.random = this.randomize();
  }

  randomize () {
    return Math.floor((Math.random() * this.state.range[1]) + this.state.range[0]);
  }

  validate (value) {
    if (!value) return ERR_EMPTY;
    return value +
      (isNaN(value) ? ERR_NOT_A_NUMBER :
        (value > this.state.range[1] || value < this.state.range[0]) ?  ERR_OUT_OF_RANGE :
        (parseInt(value) === this.random ? CORRECT : INCORRECT ));
  }

  render () {
    return <div>
      <div className="pure-g header">
        <div className="pure-u-1">
          <button className="pure-button" onClick={this.handleToggleSettings.bind(this)}>
            { this.state.showSettings ? 'Save' : 'Settings' }
          </button>
        </div>
      </div>
      { this.state.showSettings ?
        <div>
          <div className="pure-g">
            <div className="pure-u-1">Current Range {this.state.range[0]} - {this.state.range[1]}</div>
          </div>
          <Slider
            className="slider"
            range
            min={MIN}
            max={MAX}
            value={this.state.range}
            onChange={this.handleSliderChange.bind(this)} /></div>
        : null
      }
      { this.state.showSettings ? null :
        <form className="pure-form pure-form-stacked">
          <fieldset>
            <div className="pure-g">
              <div className="pure-u-1">Guess a number between {this.state.range[0]} - {this.state.range[1]}</div>
            </div>
            { this.state.status ?
              <div className="pure-g">
                <div className="pure-u-1 status">{ this.state.status }</div>
              </div> : null }
            <div className="pure-g">
              <div className="pure-u-1">
                <input
                  type="text"
                  ref="guess"
                  placeholder="Enter a guess" />
              </div>
            </div>
            <div className="pure-g">
              <div className="pure-u-1">
                <button
                  className="pure-button pure-button-primary"
                  onClick={this.handleGuess.bind(this)}>Make guess
                </button>
              </div>
            </div>
          </fieldset>
        </form>
      }
    </div>
  }
}

export default Main;
