import React                      from 'react';
import ReactDOM                   from 'react-dom';
import { Router, browserHistory } from 'react-router';
import Routes                     from './routes';
import style                      from '../sass/main.scss';

class App extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return <div>{this.props.children}</div>
  }
};

// initialize router
ReactDOM.render(
  <Router routes={Routes} history={browserHistory} />,
  document.getElementById('app')
);

export default App;
